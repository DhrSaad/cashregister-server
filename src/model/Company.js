const mongoose = require("mongoose");
const getModel = require("./model_cache");
const Schema = mongoose.Schema;
const Product = require("../model/Product").schema;

const companyschema = new Schema(
  {
    users: [
      {
        type: Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    name: {
      type: String,
      required: [true, "a Company needs a name!"],
      unique: [true, "Company already exist"],
    },
    products: [
      {
        type: Schema.Types.ObjectId,
        ref: "Product",
        required: [true, "a Company needs atleast one product!"],
      },
    ],
  },
  {
    collection: "companies",
  }
);

module.exports = getModel("Company", companyschema);
