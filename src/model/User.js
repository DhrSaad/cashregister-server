const mongoose = require("mongoose");
const getModel = require("./model_cache");
const Schema = mongoose.Schema;
const Userschema = new Schema(
  {
    email: {
      type: String,
      required: [true, "Email is required"],
      unique: [true, "User already exist"],
    },
    password: { type: String, required: [true, "Password is required"] },
    name: {
      type: String,
      required: [true, "Name is required"],

      index: true,
    },
    dateOfBirth: { type: String, required: [true, "dateOfBirth is required"] },
    isAdmin: {
      type: Boolean,
      default: true,
    },
  },
  {
    collection: "users",
  }
);

module.exports = getModel("User", Userschema);
