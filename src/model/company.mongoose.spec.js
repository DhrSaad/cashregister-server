const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
const Company = require("./Company")();
const User = require("./User")();
chai.use(chaiAsPromised);

const Product = require("./product")(); // note we need to call the model caching function

describe("Company model", function () {
  let user;
  let product;

  beforeEach(async function () {
    user = {
      name: "Yasin",
      email: "dhrsaad13@gmail.com",
      dateOfBirth: "12-12-2001",
      password: "root",
    };
    product = {
      name: "Iphone XS Max",
      price: "699.99",
      amountInStorage: "25",
    };

    const savedUser = await new User(user).save();
    const savedProduct = await new Product(product).save();
    user["id"] = savedUser._id;
    product["id"] = savedProduct._id;
  });
  describe("unit tests", function () {
    it("should reject a missing name", async function () {
      const testCompany = {};

      await expect(new Company(testCompany).validate()).to.be.rejectedWith(
        Error
      );
      it("should reject a incorrect user", async function () {
        const testCompany = {
          name: "Exxpo",
          users: 12,
          products: product._id,
        };

        await expect(new Company(testCompany).validate()).to.be.rejectedWith(
          Error
        );
      });
      it("should pass with a user, product", async function () {
        const testCompany = new Company({
          name: "Exxpo",
          users: user._id,
          products: product._id,
        });
        await testCompany.save();
        expect(testCompany).to.have.property("name", "Exxpo");
        expect(testCompany).to.have.property("users", user._id);

        expect(testCompany).to.have.property("products", product._id);
      });
      it("should reject incorrect product", async function () {
        const testCompany = {
          name: "Exxpo",
          users: user._id,
          products: 12,
        };

        await expect(new Company(testCompany).validate()).to.be.rejectedWith(
          Error
        );
      });
    });
  });
});
