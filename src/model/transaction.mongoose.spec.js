const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
const Product = require("./Product")();
const Transaction = require("./Transaction")();
chai.use(chaiAsPromised);

const User = require("./User")(); // note we need to call the model caching function

describe("Transaction model", function () {
  describe("unit tests", function () {
    let user;

    beforeEach(async function () {
      user = {
        name: "Yasin",
        email: "dhrsaad13@gmail.com",
        dateOfBirth: "12-12-2001",
        password: "root",
      };
      product = {
        name: "Iphone XS Max",
        price: "699.99",
        amountInStorage: "25",
      };

      const savedUser = await new User(user).save();
      const savedProduct = await new Product(product).save();
      user["id"] = savedUser._id;
      product["id"] = savedProduct._id;
    });
    it("should create without a product", async function () {
      // Arrange Act
      const testTransaction = new Transaction({
        user: user.id,
      });
      // Assert
      await testTransaction.save();
      console.log(testTransaction);
      expect(testTransaction.products).to.have.lengthOf(0);
      expect(testTransaction).to.have.property("amount");
      expect(testTransaction).to.have.property("isDone");
      expect(testTransaction).to.have.property("user");
    });
    it("should reject without a userid", async function () {
      // Arrange
      const testTransaction = {};
      // Act
      await expect(
        new Transaction(testTransaction).validate()
      ).to.be.rejectedWith(Error);
    });
    it("should reject with a wrong userid", async function () {
      // Arrange
      const testTransaction = {
        userid: 12345,
      };
      // Act
      await expect(
        new Transaction(testTransaction).validate()
      ).to.be.rejectedWith(Error);
    });
    it("should reject with a wrong product", async function () {
      // Arrange
      const testTransaction = {
        prodcut: 12356,
      };
      // Act
      await expect(
        new Transaction(testTransaction).validate()
      ).to.be.rejectedWith(Error);
    });
    it("should pass with a new product", async function () {
      // Arrange
      const testTransaction = new Transaction({
        user: user.id,
        products: product.id,
      });
      // Act
      await testTransaction.save();
      console.log(testTransaction);
      expect(testTransaction.products).to.have.lengthOf(1);
      expect(testTransaction).to.have.property("amount");
      expect(testTransaction).to.have.property("isDone");
      expect(testTransaction).to.have.property("user");
    });
  });
});
