const mongoose = require("mongoose");
const Product = require("../model/Product")();
const User = require("../model/User")();
const getModel = require("./model_cache");
const Schema = mongoose.Schema;
const Transactionschema = new Schema(
  {
    products: [{ type: Schema.Types.ObjectId, ref: "Product" }],
    isDone: { type: Boolean, default: false },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: [true, "a Transaction needs a User!"],
    },
  },
  {
    collection: "transactions",
  }
);

module.exports = getModel("Transaction", Transactionschema);
