const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require("./User")(); // note we need to call the model caching function

describe("User model", function () {
  describe("unit tests", function () {
    it("should reject without a email", async function () {
      const testUser = {
        name: "Yasin",
        dateOfBirth: "12-12-2001",
        password: "root",
      };

      // use validate and not save to make it a real unit test (we don't require a db this way)
      await expect(new User(testUser).validate()).to.be.rejectedWith(Error);
    });
    it("should reject without a dateOfBirth", async function () {
      const testUser = {
        email: "test@gmail.com",
        name: "Yasin",
        password: "root",
      };

      // use validate and not save to make it a real unit test (we don't require a db this way)
      await expect(new User(testUser).validate()).to.be.rejectedWith(Error);
    });

    it("should reject a missing password", async function () {
      const testUser = {
        email: "test@gmail.com",
        name: "Yasin",
        dateOfBirth: "12-12-2001",
      };

      await expect(new User(testUser).validate()).to.be.rejectedWith(Error);
    });
    it("should create a user with correct email", async function () {
      const testUser = new User({
        email: "test@gmail.com",
        name: "Yasin",
        password: "root",
        dateOfBirth: "12-12-2001",
      });
      await testUser.save();
      expect(testUser).to.have.property("email", "test@gmail.com");
    });
  });
});
