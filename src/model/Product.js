const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);
const getModel = require("./model_cache");
const Schema = mongoose.Schema;
const productschema = new Schema(
  {
    name: {
      type: String,
      required: [true, "a Company needs a name!"],
      unique: [true, "Product already exist"],
    },
    price: {
      type: Number,
      required: true,
      validate: {
        validator: (price) => price > 0,
        message: "A price needs to be positive.",
      },
    },
    amountInStorage: {
      type: Number,
      required: true,
      validate: {
        validator: (price) => price > 0,
        message: "The amount needs to be positive.",
      },
    },
  },
  {
    collection: "products",
  }
);

module.exports = getModel("Product", productschema);
