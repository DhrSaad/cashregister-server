const chai = require("chai");
const expect = chai.expect;
const requester = require("../../requester.spec");
const Product = require("../model/Product")();
const Company = require("../model/Company")();
const jwt = require("jsonwebtoken");
const { await } = require("../controllers/product.controller");
const User = require("../model/User")();
const testCompany = {
  name: "Apple",
};
const testProduct2 = {
  name: "Iphone XR Max",
  price: "499.99",
  amountInStorage: "34",
};
const testProduct = {
  name: "Iphone XS Max",
  price: "699.99",
  amountInStorage: "25",
};
const testUser = {
  email: "test@gmail.com",
  name: "Yasin",
  dateOfBirth: "12-12-2001",
  password: "root",
};
describe("product endpoints", function () {
  describe("integration tests", function () {
    it("(GET /list should return all products", async function () {
      // Arrange
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      await requester
        .post("/product/create")
        .send(testProduct2)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Act
      const res = await requester
        .get("/product/read")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      // Assert
      console.log(res.body);
      expect(res.body).to.be.an("array").to.have.lengthOf(2);

      expect(res).to.have.status(200);
    });
    it("(POST /create should add a product)", async function () {
      // Arrange // Act
      const res = await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res.body).to.have.property("_id");
      expect(res.body).to.have.property("name");
      expect(res.body).to.have.property("price");
      expect(res.body).to.have.property("amountInStorage");
    });
    it("(POST /create) should not add a product because of missing name", async function () {
      // Arrange // Act
      const res = await requester
        .post("/product/create")
        .send({
          price: "499.99",
          amountInStorage: "20",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(412);
      expect(res.body).to.have.property("error");
    });
    it("(POST /create) should not add a product because of missing header", async function () {
      // Arrange // Act
      const res = await requester.post("/product/create").send(testProduct);

      // Assert
      expect(res).to.have.status(401);
      expect(res.body).to.have.property("error");
    });
    it("(DELETE /delete should delete product)", async function () {
      // Arrange
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let product = await Product.findOne(testProduct);
      // Act
      await requester
        .delete("/product/delete")
        .send({ _id: product._id })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      product = await Product.findOne(testProduct);
      expect(product).to.be.null;
    });
    it("(DELETE /delete should give the right error code because of the missing input)", async function () {
      // Arrange act
      const res = await requester
        .delete("/product/delete")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // assert
      expect(res).to.have.status(412);
      expect(res.body).to.have.property("Error");
    });
    it("(PATCH /update) should edit the product succesfully", async function () {
      // Arrange
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let product = await Product.findOne(testProduct);
      // Act
      const res = await requester
        .put("/product/update")
        .send({
          name: "Iphone 8",
          _id: product._id,
          price: 349.99,
          amountInStorage: 25,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      // Assert
      expect(res).to.have.status(201);
      expect(res.body).to.have.property("name");
      expect(res.body).to.have.property("_id");
      expect(res.body).to.have.property("price");
    });
    it("(PATCH /update) should return error message because of missing product id", async function () {
      // Arrange
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let product = await Product.findOne(testProduct);
      // Act
      const res = await requester
        .put("/product/update")
        .send({
          name: "Iphone 8",
          price: 349.99,
          amountInStorage: 25,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      // Assert

      expect(res).to.have.status(412);
      expect(res.body).to.have.property("error");
    });
    it("(GET /find) should find designated company", async function () {
      // Arrange
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      await requester
        .post("/product/create")
        .send(testProduct2)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let product = await Product.findOne(testProduct2);
      // Act

      const res = await requester
        .get("/product/find")
        .send(product)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(200);
      expect(res.body).to.have.property("name");
      expect(res.body).to.have.property("_id");
      expect(res.body).to.have.property("price");
    });
  });
});
