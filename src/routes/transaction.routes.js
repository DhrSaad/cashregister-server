const express = require("express");
const router = express.Router();
const productcontroller = require("../controllers/product.controller");
const transactioncontroller = require("../controllers/transaction.controller");
const AuthController = require("../controllers/user.controller");
var logger = require("tracer").colorConsole();

router.post(
  "/create",

  AuthController.validateToken,
  transactioncontroller.createTransaction
);

router.delete(
  "/delete",

  AuthController.validateToken,
  transactioncontroller.deleteTransaction
);
router.put(
  "/update",
  AuthController.validateToken,
  transactioncontroller.updateTransaction
);
router.delete(
  "/removeproduct",

  AuthController.validateToken,
  transactioncontroller.removeProduct
);
router.get(
  "/read",

  AuthController.validateToken,
  transactioncontroller.readTransaction
);
router.get(
  "/getall",

  AuthController.validateToken,
  transactioncontroller.getAllTransactions
);

module.exports = router;
