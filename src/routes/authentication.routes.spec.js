const { doesNotMatch, doesNotReject } = require("assert");
const chai = require("chai");
const expect = chai.expect;
const requester = require("../../requester.spec");
const User = require("../model/User")();
const jwt = require("jsonwebtoken");
const testUser = {
  email: "test@gmail.com",
  name: "Yasin",
  dateOfBirth: "12-12-2001",
  password: "root",
};
const testUser2 = {
  email: "dhrsaad@gmail.com",
  name: "Saad",
  dateOfBirth: "12-12-2001",
  password: "root",
};
describe("user endpoints", function () {
  describe("integration tests", function () {
    it("(POST /register) should create a user with token!", async function () {
      // Act
      const res = await requester.post("/user/register").send(testUser);
      expect(res).to.have.status(201);
      // Assert
      expect(res.body).to.have.property("_id");
      expect(res.body).to.have.property("isAdmin").to.be.equal(true);
      expect(res.body).to.have.property("email");
      expect(res.body).to.have.property("name");
    });
    it("(UPDATE /update should update user details)", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      let user = await User.findOne(testUser);
      // Act
      const res = await requester
        .put("/user/update")
        .send({
          _id: user._id,
          name: "Sa'ad",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(201);
      expect(res.body).to.have.property("name", "Sa'ad");
    });
    it("(UPDATE /update should not update user details because of missing id!)", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      let user = await User.findOne(testUser);
      // Act
      const res = await requester
        .put("/user/update")
        .send({
          name: "Sa'ad",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(412);
    });
    it("(DELETE /delete should delete user!)", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      let user = await User.findOne(testUser);
      // Act
      const res = await requester
        .delete("/user/delete")
        .send({
          _id: user._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      user = await User.findOne(testUser);
      // Assert
      expect(res).to.have.status(200);
      expect(user).to.be.null;
    });

    it("(POST /register) should not create a user with details missing", async function () {
      // Arange
      const wrongUser = {
        email: "test@gmail.com",
        password: "123456",
      };
      // Act
      const res = await requester.post("/user/register").send(wrongUser);
      // Assert
      expect(res).to.have.status(412);
    });
    it("(POST /login) should obtain a  token!", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      // Act
      const res = await requester.post("/user/login").send(testUser);
      expect(res).to.have.status(200);
      expect(res.body).to.have.property("token");
      expect(res.body).to.have.property("_id");
    });
    it("(POST /login) should not login with wrong details!", async function () {
      // Arrange
      const wrongUser = {
        email: "test@gmail.com",
        password: "wrongpass",
      };
      await requester.post("/user/register").send(testUser);
      // Arrange
      const res = await requester.post("/user/login").send(wrongUser);
      // Assert
      expect(res).to.have.status(401);
      expect(res.body).to.have.property("message");
    });
    it("(GET /read should return User", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);

      let user = await User.findOne(testUser);
      // Act
      let res = await requester
        .get("/user/read")
        .send({ _id: user._id })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res.body).to.have.property("_id");
      expect(res.body).to.have.property("isAdmin").to.be.equal(true);
      expect(res.body).to.have.property("email");
      expect(res.body).to.have.property("name");
    });
    it("(GET /getall) should return all users", async function () {
      // Arrange
      let UserB = {
        email: "drsaad@gmail.com",
        name: "Saad",
        dateOfBirth: "12-12-2001",
        password: "root",
      };
      await requester.post("/user/register").send(testUser);
      await requester.post("/user/register").send(UserB);

      // Act
      const res = await requester
        .get("/user/getall")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      // Assert
      expect(res.body).to.be.an("array").to.have.lengthOf(2);
      expect(res).to.have.status(200);
    });
    it("(PATCH /setadmin) should make user admin", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      let user = await User.findOne(testUser);
      // Act
      const res = await requester
        .put("/user/setAdmin")
        .send({ id: user._id })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res.body).to.have.property("isAdmin").and.is.true;
      expect(res).to.have.status(200);
    });
    it("(PATCH /removeadmin) should remove admin!", async function () {
      await requester.post("/user/register").send(testUser);
      let user = await User.findOne(testUser);
      // Act

      const res = await requester
        .put("/user/deleteAdmin")
        .send({ id: user._id })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res.body).to.have.property("isAdmin").and.is.false;
      expect(res).to.have.status(200);
    });
    it("(POST /follow) should return with succes and a follow in Neo with a return of all following", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      await requester.post("/user/register").send(testUser2);
      const user = await User.findOne(testUser);
      const user2 = await User.findOne(testUser2);

      // Act
      let res = await requester
        .put("/user/follow")
        .send({
          userid: user._id,
          followid: user2._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(201);
      res = await requester
        .get("/user/follow")
        .send({
          userid: user._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      console.log(res.body);
      expect(res.body[0]).to.have.property("_id");
      expect(res.body[0]).to.have.property("email", user2.email);
      expect(res.body[0]).to.have.property("name", user2.name);
    });
    it("(POST /follow) should return with fail and no follow in Neo.", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      await requester.post("/user/register").send(testUser2);
      const user = await User.findOne(testUser);
      const user2 = await User.findOne(testUser2);

      // Act
      let res = await requester
        .put("/user/follow")
        .send({
          userid: user._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res.body).to.have.property("error");
      expect(res).to.have.status(412);
    });
    it("(DELETE /follow should return with succes and user is unfollowed in Neo) ", async function () {
      // Arrange
      await requester.post("/user/register").send(testUser);
      await requester.post("/user/register").send(testUser2);
      const user = await User.findOne(testUser);
      const user2 = await User.findOne(testUser2);

      await requester
        .put("/user/follow")
        .send({
          userid: user._id,
          followid: user2._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Act
      await requester
        .delete("/user/follow")
        .send({
          userid: user._id,
          followid: user2._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      // Assert

      const res = await requester
        .get("/user/follow")
        .send({
          userid: user._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      expect(res).to.have.status(200);
      expect(res.body).to.be.empty;
    });
  });
});
