const express = require("express");
const router = express.Router();
const productcontroller = require("../controllers/product.controller");
const AuthController = require("../controllers/user.controller");
var logger = require("tracer").colorConsole();
// Add product to db
router.post(
  "/create",
  AuthController.validateToken,
  productcontroller.addProduct
);
// Get list of product
router.get(
  "/read",
  AuthController.validateToken,
  productcontroller.getAllProducts
);
// Delete product
router.delete(
  "/delete",
  AuthController.validateToken,
  productcontroller.deleteProduct
);
// Update product
router.put(
  "/update",
  AuthController.validateToken,
  productcontroller.editProduct
);
// Get specific product
router.get("/find", AuthController.validateToken, productcontroller.getProduct);

module.exports = router;
