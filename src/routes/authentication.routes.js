const express = require("express");
const routes = require("express").Router();
const AuthController = require("../controllers/user.controller");

routes.post("/login", AuthController.login);
routes.post("/register", AuthController.register);
routes.delete(
  "/delete",
  AuthController.validateToken,
  AuthController.deleteUser
);
routes.put("/update", AuthController.validateToken, AuthController.updateUser);
routes.delete("/follow", AuthController.unfollowUser);
routes.get("/follow", AuthController.getFollowing);

routes.put("/follow", AuthController.followUser);
routes.put(
  "/setAdmin",

  AuthController.validateToken,
  AuthController.makeAdmin
);
routes.put(
  "/deleteAdmin",

  AuthController.validateToken,
  AuthController.deleteAdmin
);
routes.get("/getall", AuthController.validateToken, AuthController.getAllUsers);
routes.get("/info", AuthController.sendInfo);
routes.get("/read", AuthController.validateToken, AuthController.getUser);

module.exports = routes;
