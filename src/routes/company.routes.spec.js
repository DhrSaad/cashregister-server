const chai = require("chai");
const expect = chai.expect;
const requester = require("../../requester.spec");
const Product = require("../model/Product")();
const Company = require("../model/Company")();
const jwt = require("jsonwebtoken");
const User = require("../model/User")();
const testCompany = {
  name: "Apple",
};
const testCompany2 = {
  name: "Samsung",
};
const testProduct = {
  name: "Iphone XS Max",
  price: "699.99",
  amountInStorage: "25",
};
const testProduct2 = {
  name: "Iphone 21 Max",
  price: "1699.99",
  amountInStorage: "25",
};
const testUser = {
  email: "test@gmail.com",
  name: "Yasin",
  dateOfBirth: "12-12-2001",
  password: "root",
};

describe("company endpoints", function () {
  beforeEach(async function () {
    await requester
      .post("/product/create")
      .send(testProduct)
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    let product = await Product.findOne(testProduct2);
  });
  describe("integration tests", function () {
    it("(UPDATE /update) should update a company", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let company = await Company.findOne(testCompany);
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let product = await Product.findOne(testProduct);
      // Act
      const res = await requester
        .put("/company/update")
        .send({
          _id: company._id,
          name: "Starr",
          product: product._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(201);
      expect(res.body).to.have.property("name", "Starr");
    });
    it("(UPDATE /update) should not update a company with missing id", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let company = await Company.findOne(testCompany);
      // Act
      const res = await requester
        .put("/company/update")
        .send({
          name: "Starr",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(412);
      expect(res.body).to.have.property("error");
    });
    it("(READ /read) should return a company", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let company = await Company.findOne(testCompany);
      // Act
      const res = await requester
        .get("/company/read")
        .send({
          _id: company._id,
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(200);
      expect(res.body).to.have.property("name", "Apple");
    });
    it("(READ /read) Should return proper error message when given no id", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let company = await Company.findOne(testCompany);
      // Act
      const res = await requester
        .get("/company/read")
        .send({})
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(412);
      expect(res.body).to.have.property("error");
    });

    it("(POST /create) should create a company", async function () {
      return (
        requester
          .post("/company/create")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          // Arrange Act
          .send(testCompany)
          .then((res) => {
            // Assert
            expect(res).to.have.status(201);
            return Company.findOne({ name: testCompany.name });
          })
          .then((company) => {
            expect(company).to.have.property("name", testCompany.name);
          })
      );
    });
    it("(POST /create) should not create a company because of missing name", async function () {
      // Arrange Act
      const res = await requester
        .post("/company/create")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      expect(res).to.have.status(412);
    });

    it("DELETE /removeproduct should remove product from company", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      await requester
        .post("/product/create")
        .send(testProduct)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      let product = await Product.findOne(testProduct);
      let company = await Company.findOne(testCompany);
      // Act
      const res = await requester
        .delete("/company/removeproduct")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          productid: product._id,
          companyid: company._id,
        });

      // Assert
      company = await Company.findOne(testCompany);
      expect(company.products).to.be.empty;
      expect(res).to.have.status(200);
    });
    it("DELETE /removeuser should remove user from company", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      await requester
        .post("/user/register")
        .send(testUser)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      const user = await User.findOne(testUser);
      const company = await Company.findOne(testCompany);

      await requester
        .put("/company/adduser")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          useremail: user.email,
          companyname: company.name,
        });
      // Act
      const res = await requester
        .delete("/company/removeuser")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          useremail: user.email,
          companyname: company.name,
        });

      // Assert
      console.log(res.body);
      expect(res).to.have.status(200);
      expect(res.body.users).to.be.empty;
    });
    it("(DELETE /delete) should delete company", async function () {
      // Arranges
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      let company = await Company.findOne(testCompany);
      // Act
      const res = await requester
        .delete("/company/delete")
        .send(company)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Assert
      company = await Company.findOne(testCompany);

      expect(company).to.be.null;
    });
    it("GET /getall should return all companies", async function () {
      // Arrange
      await requester
        .post("/company/create")
        .send(testCompany)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      await requester
        .post("/company/create")
        .send(testCompany2)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
      // Act
      const res = await requester
        .get("/company/getall")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

      // Assert
      console.log(res.body);
      expect(res.body).to.be.lengthOf(2);
    });
  });
});
