const express = require("express");
const router = express.Router();
const companycontroller = require("../controllers/company.controller");
const AuthController = require("../controllers/user.controller");
const { route } = require("./transaction.routes");
var logger = require("tracer").colorConsole();

router.post(
  "/create",

  AuthController.validateToken,
  companycontroller.createCompany
);
router.put(
  "/update",
  AuthController.validateToken,
  companycontroller.updateCompany
);
router.delete(
  "/removeuser",
  AuthController.validateToken,
  companycontroller.removeUserFromCompany
);
router.delete(
  "/removeproduct",
  AuthController.validateToken,
  companycontroller.removeProduct
);
router.delete(
  "/delete",
  AuthController.validateToken,
  companycontroller.deleteCompany
);
router.get("/getall", AuthController.validateToken, companycontroller.getAll);
router.get("/read", AuthController.validateToken, companycontroller.getCompany);

module.exports = router;
