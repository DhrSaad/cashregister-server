const chai = require("chai");
const expect = chai.expect;
const requester = require("../../requester.spec");
const Product = require("../model/Product")();
const Company = require("../model/Company")();
const Transaction = require("../model/Transaction")();
const jwt = require("jsonwebtoken");
const { await } = require("../controllers/product.controller");
const User = require("../model/User")();
const testCompany = {
  name: "Apple",
};

const testProduct = {
  name: "Iphone XS Max",
  price: "699.99",
  amountInStorage: "25",
};
const testUser = {
  email: "test@gmail.com",
  name: "Yasin",
  dateOfBirth: "12-12-2001",
  password: "root",
};
const testUser2 = {
  email: "dhrsaad@gmail.com",
  name: "Saad",
  dateOfBirth: "12-12-2001",
  password: "root",
};
describe("transaction endpoints", function () {
  describe("integration tests", function () {});
  it("(UPDATE /update) should update transaction amount/isDone!", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    const user = await User.findOne(testUser);
    await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    const transaction = await Transaction.findOne({ user: user._id });
    // Act
    const res = await requester
      .put("/transaction/update")
      .send({
        _id: transaction._id,
        amount: 299,
        isDone: true,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    //Assert
    expect(res).to.have.status(200);
    expect(res.body).to.have.property("amount", 299);
  });
  it("(UPDATE /update) should not update transaction amount/isDone because of missing id!", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    const user = await User.findOne(testUser);
    await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    const transaction = await Transaction.findOne({ user: user._id });
    // Act
    const res = await requester
      .put("/transaction/update")
      .send({
        amount: 299,
        isDone: true,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    expect(res).to.have.status(412);
    expect(res.body).to.have.property("error");
  });
  it("(POST /create should create a transaction)", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    const user = await User.findOne(testUser);
    // Act
    const res = await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("user");
    expect(res.body).to.have.property("products");
    expect(res.body).to.have.property("isDone").to.be.equal(false);
  });
  it("(POST /create  should not create a transaction and return error with missing user.)", async function () {
    // Arrange Act
    const res = await requester
      .post("/transaction/create")
      .send({})
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    expect(res.body).to.have.property("error");
    expect(res).to.have.status(412);
  });
  it("(DELETE /deletetransaction should delete transaction)", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    await requester
      .post("/product/create")
      .send(testProduct)
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    const user = await User.findOne(testUser);
    let product = await Product.findOne(testProduct);
    await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    let transaction = await Transaction.findOne({ user: user._id });
    // Act
    const res = await requester
      .delete("/transaction/delete")
      .send(transaction)
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    transaction = await Transaction.findOne(transaction);
    // Assert
    expect(res).to.have.status(200);
    expect(res.body).to.have.property("message");
    expect(transaction).to.be.null;
  });
  it("(DELETE /removeproduct) should remove product from transaction", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    await requester
      .post("/product/create")
      .send(testProduct)
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    const user = await User.findOne(testUser);
    let product = await Product.findOne(testProduct);
    await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    let transaction = await Transaction.findOne({ user: user._id });
    await requester
      .post("/transaction/addproduct")
      .send({
        _id: transaction._id,
        productid: product._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // ACT
    const res = await requester
      .delete("/transaction/removeproduct")
      .send({
        _id: transaction._id,
        productid: product._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("user");
    expect(res.body).to.have.property("products").to.be.lengthOf(0);
    expect(res.body).to.have.property("isDone").to.be.equal(false);
  });
  it("(DELETE /removeproduct) should return error with missing value", async function () {
    // Arrange Act
    const res = await requester
      .delete("/transaction/removeproduct")
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    expect(res.body).to.have.property("error");
    expect(res).to.have.status(412);
  });
  it("(GET /read) should return a correct transaction", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    const user = await User.findOne(testUser);
    await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    let transaction = await Transaction.findOne({ user: user._id });
    // Act
    const res = await requester
      .get("/transaction/read")
      .send({
        _id: transaction._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("user");
    expect(res.body).to.have.property("products").to.be.lengthOf(0);
    expect(res.body).to.have.property("isDone").to.be.equal(false);
  });
  it("(GET /getall) should return all correct transactions", async function () {
    // Arrange
    await requester.post("/user/register").send(testUser);
    await requester.post("/user/register").send(testUser2);
    await requester
      .post("/product/create")
      .send(testProduct)
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

    const user2 = await User.findOne(testUser2);
    const user = await User.findOne(testUser);
    const product = await Product.findOne(testProduct);
    await requester
      .post("/transaction/create")
      .send({
        userid: user._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    await requester
      .post("/transaction/create")
      .send({
        userid: user2._id,
        products: product._id,
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));

    // Act
    const res = await requester
      .get("/transaction/getall")
      .send()
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
    // Assert
    console.log(res.body);
    expect(res).to.have.status(200);
    expect(res.body).to.be.lengthOf(2);
  });
});
