const assert = require("assert");
const { error } = require("console");
const e = require("express");
const jwt = require("jsonwebtoken");
var logger = require("tracer").colorConsole();
const Product = require("../model/Product")();

let controller = {
  async addProduct(req, res, next) {
    try {
      const productexist = Product.findOne({
        name: req.body.name,
        price: req.body.price,
        amountInStorage: req.body.amountInStorage,
      });
      if (productexist) {
        res.status(412).send({ error: "product does already exist" });
      } else {
        const product = new Product({
          name: req.body.name,
          price: req.body.price,
          amountInStorage: req.body.amountInStorage,
        });
        logger.log(product);
        await product.save();
        res.send(product);
      }
    } catch (err) {
      logger.error(err);
      res.status(412);
      res.send({ error: "Check your input!" });
    }
  },
  async getProduct(req, res, next) {
    try {
      if (req.body._id) {
        const product = await Product.findOne({ _id: req.body._id });
        res.send(product);
      } else if (req.body.name) {
        const product = await Product.findOne({ name: req.body.name });
      } else {
        res.status(404).send({ error: "Product doesn't exist" });
      }
    } catch {
      res.status(404);
      res.send({ error: "Product not found" });
    }
  },
  async getAllProducts(Req, res, next) {
    const product = await Product.find();
    res.status(200);
    res.send(product);
  },
  async editProduct(req, res, next) {
    try {
      const product = await Product.findOne({ _id: req.body._id });

      if (!req.body._id) {
        res.status(412).send({ error: " id is not filled in!" });
      } else {
        if (req.body.name) {
          product.name = req.body.name;
        } else if (req.body.price) {
          product.price = req.body.price;
        } else if (req.body.amountInStorage) {
          product.amountInStorage = req.body.amountInStorage;
        }

        await product.save();
        res.status(201).send(product);
      }
    } catch (err) {
      res.status(404);
      res.send(
        { error: "Something went wrong. Please check the error code" },
        err
      );
    }
  },
  async deleteProduct(req, res, next) {
    if (!req.body._id) {
      res.status(412);
      res.send({ Error: "Id is missing" });
    } else {
      try {
        const id = req.body._id;
        await Product.deleteOne({ _id: req.body._id });

        res.send("Object with id: " + id + "is deleted");
      } catch (err) {
        res.status(404);
        res.send({ error: "Product doesn't exist!" }, err);
      }
    }
  },
};
module.exports = controller;
