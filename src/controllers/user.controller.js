const assert = require("assert");
const jwt = require("jsonwebtoken");
const { getUser } = require("../../neo");
const User = require("../model/User")();
const neo = require("../../neo");
var logger = require("tracer").colorConsole();
module.exports = {
  async deleteUser(req, res, next) {
    if (!req.body._id) {
      res.status(412).send({ error: "userid is not filled in" });
    } else {
      try {
        const id = req.body._id;
        await User.deleteOne({ _id: req.body._id });

        res.send("Object with id: " + id + "is deleted");
      } catch (err) {
        res.status(404);
        res.send({ error: "User doesn't exist!" }, err);
      }
    }
  },
  async updateUser(req, res, next) {
    if (!req.body._id) {
      res.status(412).send({ error: " id is not filled in!" });
    } else {
      try {
        const user = await User.findOne({ _id: req.body._id });
        if (user.isAdmin === false) {
          res.status(401).send({ error: "No admin rights." });
        } else {
          if (req.body.email) {
            user.email = req.body.email;
          } else if (req.body.password) {
            user.password = req.body.password;
          } else if (req.body.name) {
            user.name = req.body.name;
          } else if (req.body.dateOfBirth) {
            user.dateOfBirth = req.body.dateOfBirth;
          }

          await user.save();
          res.status(201).send(user);
        }
      } catch (err) {
        res.status(404);
        console.log(err);
        res.send(
          { error: "Something went wrong. Please check the error code" },
          err
        );
      }
    }
  },
  async validateToken(req, res, next) {
    logger.info("validateToken called");
    logger.trace(req.headers);
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      res.status(401).json({
        error: "Authorization header missing!",
      });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);
      console.log(token);
      jwt.verify(token, "secret", (err, payload) => {
        if (err) {
          console.log(err);
          logger.warn("Not authorized");
          res.status(401).json({
            error: "Not authorized",
          });
        }
        if (payload) {
          logger.debug("token is valid", payload);
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint.
          req.userId = payload.id;
          next();
        }
      });
    }
  },
  async getAllUsers(req, res, next) {
    const product = await User.find();
    res.status(200);
    res.send(product);
  },
  async getFollowing(req, res, next) {
    if (!req.body.userid) {
      res.status(412).send({
        error: "Did you fill in the userid?",
      });
    } else {
      const session = neo.session();
      userid = req.body.userid;
      try {
        const result = await session.run(neo.getFollowing, {
          objectid: userid,
        });
        console.log(result);
        const objectids = result.records[0].get("objectIds");
        const followingUsers = await User.find({
          _id: { $in: objectids },
        });
        res.status(200).send(followingUsers);
        session.close();
      } catch (err) {
        console.log(err);
        return res.status(401).json({
          message: "Get User failed.",
        });
      }
    }
  },
  async followUser(req, res, next) {
    if (!req.body.userid || !req.body.followid) {
      res.status(412).send({
        error: "Did you fill in the userid or followid?",
      });
    } else {
      try {
        const session = neo.session();
        userid = req.body.userid;
        followid = req.body.followid;
        const result = await session.run(neo.createFollow, {
          userid: userid,
          followingid: followid,
        });

        res.status(201).send(result);
        session.close();
      } catch (err) {
        console.log(err);
        return res.status(401).json({
          message: "Get User failed.",
        });
      }
    }
  },
  async unfollowUser(req, res, next) {
    if (!req.body.userid || !req.body.followid) {
      res.status(412).send({
        error: "Did you fill in the userid or followid?",
      });
    } else {
      try {
        const session = neo.session();
        userid = req.body.userid;
        followid = req.body.followid;
        const result = await session.run(neo.removeFollow, {
          userid: userid,
          followingid: followid,
        });

        res.status(200).send(result);
        session.close();
      } catch (err) {
        console.log(err);
        return res.status(401).json({
          message: "Get User failed.",
        });
      }
    }
  },
  async getUser(req, res, next) {
    if (!req.body._id) {
      res.status(412).send({ error: " id is not filled in!" });
    } else {
      try {
        let user = await User.findOne({ _id: req.body._id });
        res.status(200).send(user);
      } catch (err) {
        res.status(401).send({
          message: "Get User failed.",
        });
      }
    }
  },

  sendInfo(req, res, next) {
    logger.log("Send Info function called");
    var multiStr = {
      Creator: "Sa'ad Abdullah",
      Version: "1.0.0",
      Info:
        "This is an API built by an Avans student for a fullstack project that is working in progress.",
    };
    res.send(multiStr);
  },
  async makeAdmin(req, res, next) {
    const user = await User.findOne({
      _id: req.body.id,
    })
      .then((user) => {
        user.isAdmin = true;
        user.save();
        res.status(200);
        res.send(user);
        return;
      })
      .catch((err) => {
        return res.status(401).json(err, {
          message: "Setting admin failed.",
        });
      });
  },
  async deleteAdmin(req, res, next) {
    const user = await User.findOne({
      _id: req.body._id,
    })
      .then((user) => {
        user.isAdmin = false;
        user.save();
        res.status(200);
        res.send(user);
        return;
      })
      .catch((err) => {
        return res.status(401).json(err, {
          message: "Setting admin failed.",
        });
      });
  },
  async login(req, res, next) {
    console.log("login() called");
    if (!req.body) {
      res.status(401);
      res.send({
        message: "Have u filled in everything correctly?",
      });
    }

    let getUser;

    console.log(req.body);

    await User.findOne({
      email: req.body.email,
    })
      .then((user) => {
        console.log("user found!");
        console.log(user); // USER HIER EMPTY IN CONSOLE VAN API
        // if (!user) {
        //     return res.status(401).json({
        //         message: "Authentication failed. user empty"
        //     });
        // }
        const password = req.body.password;
        getUser = user;
        if (password == user.password) {
          return "Password is accepted";
        }
      })
      .then((response) => {
        console.log("Response called!");
        console.log(response);
        if (!response) {
          return res.status(401).json({
            message: "Authentication failed.",
          });
        }
        let jwtToken = jwt.sign(
          {
            email: getUser.email,
            userId: getUser._id,
          },
          "secret",
          {
            expiresIn: "1h",
          }
        );
        res.status(200).json({
          token: jwtToken,
          expiresIn: 3600,
          _id: getUser._id,
        });
      })
      .catch((err) => {
        console.log(err);
        return res.status(401).json({
          message: "Authentication failed.",
        });
      });
  },

  async register(req, res, next) {
    if (
      !req.body.email ||
      !req.body.password ||
      !req.body.dateOfBirth ||
      !req.body.name
    ) {
      res.status(412);
      res.send("Error: The body is not filled in correctly");
    } else {
      console.log("register() called.");
      const session = neo.session();
      /**
       * Query the database to see if the email of the user to be registered already exists.
       */
      try {
        user = await User.findOne({
          email: req.body.email,
        });
        if (user) {
          console.log("User already exist!");
          return res.status(401).json({ message: "User already exist!" });
        }
        const newuser = new User(req.body);
        newuser.save();
        let userneo = await session.run(neo.getUser, {
          id: newuser._id.toString(),
        });

        const singleRecord = userneo.records[0];

        if (!singleRecord) {
          var result = await session.run(neo.createUser, {
            objectid: newuser._id.toString(),
            name: newuser.name.toString(),
          });

          session.close();
        }
        console.log(result);
        res.status(201);
        res.send(newuser);
      } catch (err) {
        res.status(404);
        logger.log(err);
        res.send("error: " + err);
      }
    }
  },
};
