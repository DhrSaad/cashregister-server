const assert = require("assert");
const { error } = require("console");
const jwt = require("jsonwebtoken");
const User = require("../model/User")();
var logger = require("tracer").colorConsole();
const Company = require("../model/Company")();
const Product = require("../model/Product")();

let controller = {
  async updateCompany(req, res, next) {
    try {
      if (!req.body._id) {
        res.status(412).send({ error: " id is not filled in!" });
      } else {
        const company = await Company.findOne({ _id: req.body._id });
        if (req.body.name) {
          company.name = req.body.name;
        } else if (req.body.product) {
          await company.products.push(req.body.product);
          let product = await Product.findOne(req.body.product);
          company.amount = company.amount + product.price;
          company.save();
        } else if (req.body.user) {
          await company.users.push(req.body.user);
        } else {
          await company.save();
          res.status(201).send(company);
        }
      }
    } catch (err) {
      res.status(404);
      res.send(
        { error: "Something went wrong. Please check the error code" },
        err
      );
    }
  },
  async getCompany(req, res, next) {
    if (!req.body._id) {
      res.status(412).send({ error: " id is not filled in!" });
    } else {
      let company = await Company.findOne({
        _id: req.body._id,
      });
      res.status(200).send(company);
      try {
      } catch (err) {
        res.status(404);
        res.send(
          { error: "Something went wrong. Please check the error code" },
          err
        );
      }
    }
  },

  async removeUserFromCompany(req, res, next) {
    if (!req.body.companyname || !req.body.useremail) {
      res.status(412);
      res.send({ error: "companyname/useremail hasn't been filled in!" });
    } else {
      try {
        const company = await Company.findOne({ name: req.body.companyname });
        const user = await User.findOne({ email: req.body.useremail });
        await company.users.pop(user);
        company.save();

        res.send(company);
      } catch (err) {
        logger.error(err);
        res.status(404);
        res.send(err, {
          error:
            "Something went wrong with removing user from company. Check the error message.",
        });
      }
    }
  },

  async createCompany(req, res, next) {
    if (!req.body.name) {
      res.status(412);
      res.send({ error: "Name is missing." });
    } else {
      const companyexist = Company.findOne({
        name: req.body.name,
      });
      if (companyexist) {
        res.status(412).send({ error: "company does already exist" });
      }
      try {
        if (req.body.products) {
          const company = new Company({
            products: req.body.products,
            name: req.body.name,
          });
        }

        const company = new Company({
          name: req.body.name,
        });
        logger.log("Company: ", company);
        await company.save();
        res.status(201);
        res.send(company);
      } catch (err) {
        logger.error(err);
        res.status(404);
        res.send(err, {
          error:
            "Something went wrong with saving company. Check the error message.",
        });
      }
    }
  },

  async removeProduct(req, res, next) {
    try {
      const company = await Company.findOne({ _id: req.body.companyid });
      const product = await Product.findOne({ _id: req.body.productid });
      if ((company.products.length = 0)) {
        res.send("Oops nothing to remove!");
      } else {
        await company.products.pop(product);
        if (company.amount > 0) {
          company.amount = company.amount - product.price;
        } else if (company.amount <= 0) {
          company.amount = 0;
        } else {
          company.save();
          res.send("Succesfully removed Product");
        }
      }
    } catch (err) {
      res.status(404);
      console.log(err);
      res.send({
        error: "Product doesn't exist! Did you check your company details?",
      });
    }
  },
  async getAll(req, res, next) {
    const company = await Company.find();
    res.status(200);
    res.send(company);
  },
  async deleteCompany(req, res, next) {
    const company = Company.findOne({ _id: req.body._id });
    if (company) {
      try {
        const id = req.body._id;

        await Company.deleteOne({ _id: req.body._id });

        res.send("Company with id: " + id + " is deleted");
      } catch {
        res.status(404);
        res.send({ error: "Something went wrong. Please check the logs" });
      }
    } else {
      res.status(404);
      res.send({ error: "company doesn't exist!" });
    }
  },
};

module.exports = controller;
