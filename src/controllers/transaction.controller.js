const assert = require("assert");
const { json } = require("body-parser");
const jwt = require("jsonwebtoken");
const Product = require("../model/Product")();
var logger = require("tracer").colorConsole();
const Transaction = require("../model/Transaction")();

let controller = {
  async getAllTransactions(Req, res, next) {
    const product = await Transaction.find();
    res.status(200);
    res.send(product);
  },
  async updateTransaction(req, res, next) {
    if (!req.body._id) {
      res.status(412).send({ error: " id is not filled in!" });
    } else {
      try {
        const transaction = await Transaction.findOne({
          _id: req.body._id,
        });
        if (req.body.amount) {
          transaction.amount = req.body.amount;
        } else if (req.body.isDone) {
          transaction.isDone = req.body.isDone;
        } else if (req.body.product) {
          await transaction.products.push(req.body.product);
        }

        transaction.save();
        res.status(200).send(transaction);
      } catch (err) {
        logger.error(err);
        res.status(404);
        res.send({ error: "Check your input!" });
      }
    }
  },
  async createTransaction(req, res, next) {
    if (!req.body.userid) {
      res.status(412).send({ error: "No user!" });
    } else {
      try {
        const transaction = new Transaction({
          amount: req.body.amount,
          products: req.body.products,
          isDone: req.body.isDone,
          user: req.body.userid,
        });
        logger.log(transaction);
        await transaction.save();
        res.send(transaction);
      } catch (err) {
        logger.error(err);
        res.status(404);
        res.send({ error: "Check your input!" });
      }
    }
  },

  async addProduct(req, res, next) {
    if (!req.body._id || !req.body.productid) {
      res.status(412).send({ error: "No Product or Transaction!" });
    } else {
      try {
        const transaction = await Transaction.findOne({ _id: req.body._id });
        const product = await Product.findOne({ _id: req.body.productid });

        var amountincreased = await product.get("price");
        console.log("price:", amountincreased);
        await transaction.products.push(product);

        let subtotal = transaction.amount;
        let total = subtotal + amountincreased;
        transaction.amount = total;

        transaction.save();

        res.status(201).send(transaction);
      } catch (err) {
        logger.error(err);
        res.status(404);
        res.send({ error: "Oops something went wrong! check logs" });
      }
    }
  },
  async deleteTransaction(req, res, next) {
    try {
      const id = req.body._id;
      await Transaction.deleteOne({ _id: req.body._id });

      res.status(200).send({ message: "Object is deleted" });
    } catch {
      res.status(404);
      res.send({ error: "Transaction doesn't exist!" });
    }
  },
  async removeProduct(req, res, next) {
    if (!req.body._id || !req.body.productid) {
      res.status(412).send({ error: "No user or Transaction!" });
    } else {
      try {
        const transaction = await Transaction.findOne({ _id: req.body._id });
        const product = await Product.findOne({ _id: req.body.productid });
        if ((transaction.get("products").length = 0)) {
          res.send("Oops nothing to remove!");
        }
        await transaction.products.pop(product._id);

        if (transaction.amount == 0) {
          transaction.save();
          res.status(201).send(transaction);
        } else if (transaction.amount < 0) {
          transaction.amount = 0;
          transaction.save();
          res.status(201).send(transaction);
        } else {
          var decrease = await product.get("price");
          let subtotal = transaction.amount;
          let total = subtotal - decrease;
          transaction.amount = total;

          transaction.save();
          res.status(200).send(transaction);
        }
      } catch (err) {
        res.status(404);
        console.log(err);
        res.send({
          error: "Product doesn't exist! Did you check your transaction?",
        });
      }
    }
  },
  async readTransaction(req, res, next) {
    try {
      const transaction = await Transaction.findOne({ _id: req.body._id });
      res.status(200).send(transaction);
    } catch (err) {
      res.status(404);
      res.send({
        error: "Transaction doesn't exist!",
      });
    }
  },
};

module.exports = controller;
