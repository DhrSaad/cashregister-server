const chai = require("chai");
const expect = chai.expect;
const Product = require("./model/Product")();
const User = require("./model/User")();
const Company = require("./model/Company")();
const Transaction = require("./model/Transaction")();
const jwt = require("jsonwebtoken");

const requester = require("../requester.spec");

it("create user/company; add user to company, create product; user creates transaction; user adds product to transaction", async function () {
  let res;

  const testUser = {
    email: "test@gmail.com",
    name: "Yasin",
    password: "root",
    dateOfBirth: "12-12-2001",
  };
  res = await requester.post("/user/register").send(testUser);
  expect(res).to.have.status(201);
  expect(res.body).to.have.property("email", testUser.email);
  expect(res.body).to.have.property("name", testUser.name);
  expect(res.body).to.have.property("dateOfBirth", testUser.dateOfBirth);

  testUser._id = res.body._id;

  const testProduct = {
    name: "Iphone XS Max",
    price: "699.99",
    amountInStorage: "25",
  };

  res = await requester
    .post("/product/create")
    .send(testProduct)
    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
  expect(res).to.have.status(200);
  expect(res.body).to.have.property("name", testProduct.name);
  expect(res.body.price).to.be.an("number");

  testProduct._id = res.body._id;
  const testCompany = {
    name: "Albert Heijn",
    products: testProduct._id,
  };
  res = await requester
    .post("/company/create")
    .send(testCompany)
    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
  expect(res).to.have.status(201);
  testCompany._id = res.body._id;
  expect(res.body).to.have.property("name", testCompany.name);
  res = await requester
    .put("/company/update")
    .send({
      _id: testCompany._id,
      user: testUser._id,
    })
    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
  testCompany._id = res.body._id;
  expect(res).to.have.status(201);
  expect(res.body.users).to.have.lengthOf(1);
  expect(res.body).to.have.property("name", testCompany.name);

  const testTransaction = {
    userid: testUser._id,
  };

  res = await requester
    .post("/transaction/create")
    .send(testTransaction)
    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
  expect(res).to.have.status(200);
  expect(res.body).to.have.property("user", testUser._id);
  testTransaction._id = res.body._id;

  res = await requester
    .put("/transaction/update")
    .send({
      _id: testTransaction._id,
      product: testProduct._id,
    })
    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"));
  console.log(res.body);
  expect(res.body.products).to.have.lengthOf(1);
  expect(res.body).to.have.property("amount");
  expect(res.body).to.have.property("user", testUser._id);
});
