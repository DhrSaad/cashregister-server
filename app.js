const express = require("express");

// this catches an exception in a route handler and calls next with it,
// so express' error middleware can deal with it
// saves us a try catch in each route handler
// note: this will be standard in express 5.0, to be released soon
require("express-async-errors");

const app = express();

// parse json body of incoming request
app.use(express.json());

const productroutes = require("./src/routes/product.routes");
const transactionroutes = require("./src/routes/transaction.routes");
const authroutes = require("./src/routes/authentication.routes");
const companyroutes = require("./src/routes/company.routes");

const errors = require("./errors");
app.use("/product", productroutes); // new
app.use("/transaction", transactionroutes);
app.use("/company", companyroutes);
app.use("/user", authroutes);

// catch all not found response
app.use("*", function (_, res) {
  res.status(404).end();
});

// error responses
app.use("*", function (err, req, res, next) {
  console.error(`${err.name}: ${err.message}`);
  // console.error(err)
  next(err);
});

app.use("*", errors.handlers);

app.use("*", function (err, req, res, next) {
  res.status(500).json({
    message: "something really unexpected happened",
  });
});

// export the app object for use elsewhere
module.exports = app;
