// reads the .env file and stores it as environment variables, use for config
require("dotenv").config();

const connect = require("./connect");

const User = require("./src/model/User")(); // note we need to call the model caching function
const Product = require("./src/model/Product")(); // note we need to call the model caching function
const Company = require("./src/model/Company")();
const neo = require("./neo");
const Transaction = require("./src/model/Transaction")();

// connect to the databases
connect.mongo(process.env.MONGO_TEST_DB);
connect.neo(process.env.NEO4J_TEST_DB);

beforeEach(async () => {
  // drop both collections before each test
  await Promise.all([
    User.deleteMany(),
    Product.deleteMany(),
    Company.deleteMany(),
    Transaction.deleteMany(),
  ]);

  // clear neo db before each test
  const session = neo.session();
  await session.run(neo.dropAll);
  await session.close();
});
