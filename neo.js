const neo4j = require("neo4j-driver");

function connect(dbName) {
  this.dbName = dbName;
  this.driver = neo4j.driver(
    process.env.NEO4J_URL,
    neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
  );
}

function session() {
  return this.driver.session({
    database: this.dbName,
    defaultAccessMode: neo4j.session.WRITE,
  });
}

module.exports = {
  connect,
  session,
  dropAll: "MATCH (n) DETACH DELETE n",
  getUser: "MATCH (n:User {objectid: $id}) RETURN n.name",
  createUser: "CREATE (u:User {objectid: $objectid, name: $name}) RETURN u",
  createFollow:
    "MATCH (u1 {objectid: $userid})  MATCH (u2 {objectid: $followingid}) CREATE (u1)-[r:FOLLOWING]->(u2) return u1",
  removeFollow:
    "MATCH (:User {objectid: $userid})-[r:FOLLOWING]-(:User {objectid: $followingid}) DELETE r",
  getFollowing:
    "MATCH (a:User {objectid: $objectid})-[r]-(b) RETURN collect(DISTINCT b.objectid) as objectIds",
  //  MATCH (saad {objectid:"5fce35394c75f8775044e21e"})  MATCH (menno {objectid: "5fce35394c75f8a75053e232e"}) CREATE (saad)-[:FOLLOWING]->(menno)
};
